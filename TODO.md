Sora's Todo List
--- 
- [x] Aesthetics
    - [x] Add Derelict Ship
        - [x] Find model
        - [x] Configure Model
        - [x] Configure AABB
- [ ] Make sure NanoVG get's cleaned up
- [ ] Abstract NanoVG into classes
- [ ] Radio logs
    - [ ] Get Audio recordings
        - [x] Temporary Audio recordings
    - [x] Find Radio models
    - [x] Configure Radio models
    - [x] Update collision system to have an "OnCollision" callback
    - [ ] Create a Radio UI
    - [ ] Subtitles?
- [ ] Find a way to generate Radios

Bugs
===
