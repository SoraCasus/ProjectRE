Project RE
===
This is a project, currently in beta phase.

Download
===
Download Here: [Rutikal Escape 0.2.0a.jar](/out/artifacts/Rutikal_Escape_0_2_0a/Rutikal Escape 0.2.0a.jar)

Changelog
===
- Implemented game states
    - Menu State
    - Game State
    - Rutikal Codex State with lots of information!
- Implemented UI
    - Buttons
    - Boxes
    - Text
    - TextBoxes
- Added Rutikal Codex
    - **Behold! The Lore!**
