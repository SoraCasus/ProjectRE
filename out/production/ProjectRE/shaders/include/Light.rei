struct Light {
    vec3 position;
    vec3 colour;
    vec3 attenuation;
};