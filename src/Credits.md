Credits
===
Lead Creator: Joshua "SoraCasus" Mayer
Co-Writer: Mark Newfur

Voice Log 1: Jack "Red" Regier

3D Models:  https://opengameart.org<Br>

Large Freighter Ship: <Br>
&emsp; Creator: https://twitter.com/FireBoltStudio <Br>
&emsp; Downloaded From: https://fireboltstudios.itch.io/oxar-light-freighter <Br>
&emsp; Licensed under: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) <Br>
&emsp; Edits: Added green colour to diffuse texture

