package com.soracasus.projectre.core;

public interface IDisposable {

	void delete();

}
