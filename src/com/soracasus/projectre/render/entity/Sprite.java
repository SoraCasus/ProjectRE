package com.soracasus.projectre.render.entity;

public class Sprite {

	private static final float[] vertices = {
		 1.0F,  1.0F, 0.0F,
		-1.0F, 	1.0F, 0.0F,
		-1.0F, -1.0F, 0.0F,
		 1.0F, -1.0F, 0.0F
	};

	private static final float[] textures = {
		1.0F, 0.0F,
		0.0F, 0.0F,
		0.0F, 1.0F,
		1.0F, 1.0F
	};




}
