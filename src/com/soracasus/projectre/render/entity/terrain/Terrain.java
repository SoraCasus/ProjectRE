package com.soracasus.projectre.render.entity.terrain;

import com.soracasus.projectre.render.globjects.Vao;
import com.soracasus.projectre.render.model.Material;
import com.soracasus.projectre.render.texture.Texture;
import org.joml.Matrix4f;

public class Terrain {

	private static final float SIZE = 30;
	private static final int VERTEX_COUNT = 128;

	private float x;
	private float z;
	private Vao vao;
	private Material material;
	private Matrix4f transform;

	public Terrain (int gridX, int gridZ, Material material) {
		this.material = material;
		this.x = gridX * SIZE;
		this.z = gridZ * SIZE;
		this.transform = new Matrix4f();
		transform.translate(x, 0, z);
		this.vao = generateTerrain();
	}

	private Vao generateTerrain () {
		int count = VERTEX_COUNT * VERTEX_COUNT;
		float[] vertices = new float[count * 3];
		float[] normals = new float[count * 3];
		float[] textures = new float[count * 2];
		int[] indices = new int[6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1)];
		int vertexPointer = 0;

		for (int i = 0; i < VERTEX_COUNT; i++) {
			for (int j = 0; j < VERTEX_COUNT; j++) {
				vertices[vertexPointer * 3] = (float) j / ((float) VERTEX_COUNT - 1) * SIZE;
				vertices[vertexPointer * 3 + 1] = 0;
				vertices[vertexPointer * 3 + 2] = (float) i / ((float) VERTEX_COUNT - 1) * SIZE;

				normals[vertexPointer * 3] = 0;
				normals[vertexPointer * 3 + 1] = 1;
				normals[vertexPointer * 3 + 2] = 0;

				textures[vertexPointer * 2] = (float) j / ((float) VERTEX_COUNT - 1);
				textures[vertexPointer * 2 + 1] = (float) i / ((float) VERTEX_COUNT - 1);
				vertexPointer++;
			}
		}

		int pointer = 0;
		for (int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
			for (int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
				int topLeft = (gz * VERTEX_COUNT) + gx;
				int topRight = topLeft + 1;
				int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
				int bottomRight = bottomLeft + 1;
				indices[pointer++] = topLeft;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = topRight;
				indices[pointer++] = topRight;
				indices[pointer++] = bottomLeft;
				indices[pointer++] = bottomRight;
			}
		}

		Vao vao = new Vao();
		vao.bind(0, 1, 2);
		vao.createIndexBuffer(indices);
		vao.createAttribute(0, 3, vertices);
		vao.createAttribute(1, 2, textures);
		vao.createAttribute(2, 3, normals);
		vao.unbind(0, 1, 2);

		return vao;
	}

	public float getX () {
		return x;
	}

	public float getZ () {
		return z;
	}

	public Vao getVao () {
		return vao;
	}

	public Material getMaterial () {
		return material;
	}

	public Matrix4f getTransform () {
		return this.transform;
	}
}
